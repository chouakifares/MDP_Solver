from Classes import *

mdp = MDP.create_mdp(
    env_size = (10,5),
    reward_states = [(9,4)],
    deterministic = False,
    multi_objective = True,
    r = (20,10)
)
policy, nb_iter = mdp.solve(0.99)


nb_trials = 20
scores = []
for _ in range(nb_trials):
    score = mdp.play_policy(policy = policy, nb_obj=2)
    scores.append((score[0],score[1]))

print(f"Average score in {nb_trials} attempts = ", scores)
