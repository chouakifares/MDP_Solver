from Classes import *
import time
import matplotlib.pyplot as plt


########### EX1 Q2.1 ################
size = (20, 15)
gammas = [0.9, 0.7, 0.5]
nb_trials = 1
avg_solving_times = []
avg_nb_iter = []
for gamma in gammas:
    tmp = []
    tmp_iter = []
    for _ in range(nb_trials):
        mdp = MDP.create_mdp(size,[(size[0]-1,size[1]-1)], False, False)
        start_t = time.time()
        policy, nb_iter = mdp.solve(gamma)
        time_spent = time.time() - start_t
        tmp.append(time_spent)
        tmp_iter.append(nb_iter)
    
    avg_solving_times.append(sum(tmp)/nb_trials)
    avg_nb_iter.append(sum(tmp_iter)/nb_trials)


for i,gamma in enumerate(gammas):
    print(f"{gamma}  | time --> {avg_solving_times[i]} s")
    print(f"{gamma}  | iter --> {avg_nb_iter[i]} iterations")
