import pygame as pg
import numpy as np
from cplex import *
from docplex.mp.model import Model
import sys

class State:
    def __init__(self, x, y, reward, circle_color="white",obstacle=False):
        self.x = x
        self.y = y
        self.circle_color = circle_color
        self.reward = reward
        self.obstacle = obstacle

class MDP:
    def __init__(
        self,
        state_set,
        action_set,
        reward_states,
        env_size,
        deterministic = False,
        multi_objective = False,
        nb_objectives = 1,
        cell_size = 50,
        color_obstacle=(0,0,0),
        color_goal_state = (30,255,30),
        color_normal_state = (255,255,255)
    ):
        self.state_set = state_set
        self.action_set = action_set
        self.reward_states = reward_states
        self.env_size = env_size
        self.deterministic = deterministic
        self.multi_objective = multi_objective
        self.nb_objectives = nb_objectives
        self.cell_size = cell_size
        self.color_obstacle = color_obstacle
        self.color_goal_state = color_goal_state
        self.color_normal_state = color_normal_state

    def __get_state_indices__(self,state):
        for i in range(len(self.state_set)):
            for j in range(len(self.state_set[i])):
                if(self.state_set[i][j] == state):
                    return (i,j)

    def __get_state_action_predecessors__(self, state):
        preds = []
        for i in range(len(self.state_set)):
            for j in range(len(self.state_set[i ])):
                for a in range(len(self.action_set)):
                    tmp_transitions = self.transition(
                        self.state_set[i][j], self.action_set[a]
                    )
                    if(tmp_transitions is not None):
                        for t in tmp_transitions:
                            if(state == t[0]):
                                preds.append(((i,j), a, t[1]))

        return preds
    @staticmethod
    def create_mdp(
        env_size,
        reward_states,
        deterministic,
        multi_objective,
        r = (1000,),
        p_wall=0.15,
        p_white = 0.45,
        p_green = 0.1,
        p_blue = 0.1,
        p_red = 0.1,
        seed = None,
    ):

        # grid seed:
        np.random.seed(seed = seed)

        if(not multi_objective):
            state_set = []
            for i in range(env_size[0]):
                tmp = []
                for j in range(env_size[1]):
                    if((i,j) in reward_states):
                        tmp.append(State(i, j, r))
                    else:
                        z=np.random.uniform(0,1)
                        if z < p_wall:
                            tmp.append(State(i, j, (0,),obstacle= True))
                        elif z < p_wall+ p_white:
                            tmp.append(State(i, j, (0,)))
                        elif z < p_wall+ p_white + p_green:
                            tmp.append(State(i, j, (-2,),circle_color="green"))
                        elif z < p_wall+ p_white +p_green + p_blue:
                            tmp.append(State(i, j, (-4,),circle_color="blue"))
                        elif z< p_wall + p_white + p_green + p_blue +p_red:
                            tmp.append(State(i, j, (-8,),circle_color="red"))
                        else:
                            tmp.append(State(i, j, (-16,),circle_color="black"))

                state_set.append(tmp)
        else:
            state_set = []
            for i in range(env_size[0]):
                tmp = []
                for j in range(env_size[1]):
                    if((i,j) in reward_states):
                        tmp.append(State(i, j, r))
                    else:
                        z=np.random.uniform(0,1)
                        if z < p_wall:
                            tmp.append(State(i, j, (0,0),obstacle= True))
                        elif z < p_wall+ p_white:
                            tmp.append(State(i, j, (-1,-1)))
                        elif z < p_wall+ p_white+p_blue:
                            tmp.append(State(i, j, (-2,0),circle_color="blue"))
                        else:
                            tmp.append(State(i, j, (0,-2),circle_color="red"))
                state_set.append(tmp)

        state_set[0][0].obstacle = False
        state_set[0][0].circle_color = "white"
        state_set[0][1].obstacle = False
        state_set[0][1].circle_color = "white"
        state_set[2][0].obstacle = False
        state_set[2][0].circle_color = "white"
        state_set[env_size[0]-1][env_size[1]-1].obstacle = False
        state_set[env_size[0]-1][env_size[1]-1].circle_color = "white"
        state_set[env_size[0]-2][env_size[1]-1].obstacle = False
        state_set[env_size[0]-2][env_size[1]-1].circle_color = "white"
        state_set[env_size[0]-1][env_size[1]-2].obstacle = False
        state_set[env_size[0]-1][env_size[1]-2].circle_color = "white"
        if multi_objective:
            nb_objectives = 2
        else:
            nb_objectives = 1
        np.random.seed(None)
        return MDP(
            state_set,
            ['f','g','h','j','y','u','t','r'],
            reward_states,
            env_size,
            deterministic,
            multi_objective,
            nb_objectives
        )

    def solve(self, gamma, delta = 1e-3, force_pure_policies = False):
        nb_iter = 0
        if(not self.multi_objective):
            values, new_values = [], []
            policy = []
            for i in range(len(self.state_set)):
                tmp, tmp_n, tmp_p = [], [], []
                for j in range(len(self.state_set[i])):
                    tmp_p.append(0)
                    tmp.append(0)
                    tmp_n.append(0)
                values.append(tmp)
                new_values.append(tmp_n)
                policy.append(tmp_p)
            done = False
            nb_iter = 0
            while(not done):
                nb_iter += 1
                max_diff = 0
                for i in range(len(self.state_set)):
                    for j in range(len(self.state_set[i])):
                        v = values[i][j]
                        action_values = []
                        if(not self.state_set[i][j].obstacle
                            and (i,j) not in self.reward_states):
                            tmp_actions=[]
                            for a in self.action_set:
                                r_s_a = self.state_set[i][j].reward[0] - 1
                                possible_states = self.transition(
                                    self.state_set[i][j], a
                                )
                                if(possible_states is not None):
                                    tmp_actions.append(a)
                                    future_reward = 0
                                    for (s_prim, proba) in possible_states:
                                        ind_s_prim = self.__get_state_indices__(
                                            s_prim
                                        )
                                        future_reward += (proba
                                            * values[ind_s_prim[0]][ind_s_prim[1]])
                                    future_reward *= gamma
                                    action_values.append(r_s_a + future_reward)
                            if(len(action_values)):
                                values[i][j] = max(action_values)
                                policy[i][j] = [(tmp_actions[np.argmax(action_values)], 1)]

                            max_diff = max(max_diff, abs(v - values[i][j]))
                            if(max_diff < delta):
                                done = True



        else:
            m = Model()
            X_vars, D_vars = [],[]
            z = m.continuous_var(name = f"z")
            mu = 1/(len(self.state_set))
            nb_x = 0
            for i in range(len(self.state_set)):
                tmp_x, tmp_d = [], []
                for j in range(len(self.state_set[i])):
                    tmp_state, tmp = [], []
                    for a in self.action_set:
                        nb_x += 1
                        tmp_state.append(m.continuous_var(name = f"x_{i}_{j}_{a}", lb =0))
                        tmp.append(m.binary_var(name = f"d_{i}_{j}_{a}"))
                    tmp_x.append(tmp_state)
                    tmp_d.append(tmp)

                X_vars.append(tmp_x)
                D_vars.append(tmp_d)
            for i in range(len(self.state_set)):
                for j in range(len(self.state_set[i])):
                    tmp_const_xsa = 0
                    tmp_const_dsa = 0
                    if(force_pure_policies):
                        for a in range(len(self.action_set)):
                            m.add_constraint(X_vars[i][j][a] * (1-gamma) <= D_vars[i][j][a])

                    for (i_bis,j_bis), a, proba in self.__get_state_action_predecessors__(
                            self.state_set[i][j]
                    ):
                        tmp_const_xsa += proba*X_vars[i_bis][j_bis][a]
                    if(force_pure_policies):
                        m.add_constraint(sum(D_vars[i][j]) <= 1)
                    m.add_constraint(
                        (sum(X_vars[i][j]) - gamma * tmp_const_xsa) == 1/ nb_x
                    )

            for objective_ind in range(self.nb_objectives):
                objective = 0

                for i in range(len(self.state_set)):
                    for j in range(len(self.state_set[i])):
                        objective += self.state_set[i][j].reward[objective_ind] * sum(X_vars[i][j])
                m.add_constraint(z <= objective)

            m.maximize(z)
            m.export("test.lp")
            s = m.solve()
            print(s)
            policy = []
            for objective_ind in range(self.nb_objectives):
                objective = 0
                for i in range(len(self.state_set)):
                    tmp_p = []
                    for j in range(len(self.state_set[i])):
                        p = []
                        tmp_sum = sum(s.get_value(f"x_{i}_{j}_{a}") for a in self.action_set)
                        for a in self.action_set:
                            p.append((a, s.get_value(f"x_{i}_{j}_{a}")/ tmp_sum))

                        tmp_p.append(p)
                    policy.append(tmp_p)

        return policy, nb_iter

    def transition(self, state, action):
        def generate_stochastic_state_set(state):
            if(self.deterministic):
                return [(state, 1)]

            i, j = state.x, state.y
            possible_states = []
            if(i > 0):
                if(not self.state_set[i-1][j].obstacle):
                    possible_states.append(self.state_set[i-1][j])
                if(j > 0):
                    if(not self.state_set[i][j-1].obstacle):
                        possible_states.append(self.state_set[i][j-1])
                    if(not self.state_set[i-1][j-1].obstacle):
                        possible_states.append(self.state_set[i-1][j-1])
                if(j < self.env_size[1]-1):
                    if(not self.state_set[i][j+1].obstacle):
                        possible_states.append(self.state_set[i][j+1])
                    if(not self.state_set[i-1][j+1].obstacle):
                        possible_states.append(self.state_set[i-1][j+1])

            if(i < self.env_size[0]-1):
                if(not self.state_set[i+1][j].obstacle):
                    possible_states.append(self.state_set[i+1][j])
                if(j > 0):
                    if(not self.state_set[i+1][j-1].obstacle):
                        possible_states.append(self.state_set[i+1][j-1])
                if(j < self.env_size[1]-1):
                    if(not self.state_set[i+1][j+1].obstacle):
                        possible_states.append(self.state_set[i+1][j+1])

            distribution = [(i, 0.0625) for i in possible_states]
            distribution.append((state, 1 - len(distribution)/16))
            return distribution

        i, j  = state.x, state.y
        if(action == "y"
            and i > 1
            and j < self.env_size[1]-1
            and not self.state_set[i-2][j+1].obstacle
        ):
            return generate_stochastic_state_set(self.state_set[i-2][j+1])
        elif(action == "t"
            and i > 1
            and j > 0
            and not self.state_set[i-2][j-1].obstacle
        ):
            return generate_stochastic_state_set(self.state_set[i-2][j-1])

        elif(action == "u"
            and i > 0
            and j < self.env_size[1] - 2
            and not self.state_set[i-1][j+2].obstacle
        ):
            return generate_stochastic_state_set(self.state_set[i-1][j+2])

        elif(action == "r"
            and i > 0
            and j > 1
            and not self.state_set[i-1][j-2].obstacle
        ):
            return generate_stochastic_state_set(self.state_set[i-1][j-2])

        elif(action == "h"
            and i < self.env_size[0] - 2
            and j < self.env_size[1] - 1
            and not  self.state_set[i+2][j+1].obstacle
        ):
            return generate_stochastic_state_set(self.state_set[i+2][j+1])

        elif(action == "g"
            and i < self.env_size[0] - 2
            and j > 0
            and not  self.state_set[i+2][j-1].obstacle
        ):
            return generate_stochastic_state_set(self.state_set[i+2][j-1])

        elif(action == "j"
            and i < self.env_size[0] - 1
            and j < self.env_size[1] -2
            and not  self.state_set[i+1][j+2].obstacle
        ):
            return generate_stochastic_state_set(self.state_set[i+1][j+2])

        elif(action == "f"
            and i < self.env_size[0] - 1
            and j > 1
            and not self.state_set[i+1][j-2].obstacle
        ):
            return generate_stochastic_state_set(self.state_set[i+1][j-2])


    def display_env(self, policy = None, starting_state = None, nb_obj =1):
        OFFSET = 100
        def drawGrid(screen,rectangles):
            screen.fill((192,192,192))
            for rect, color, circle_color in rectangles:
                pg.draw.rect(screen, color, rect)
                if(circle_color):
                    pg.draw.circle(
                        screen,
                        circle_color,
                        (rect.centerx, rect.centery),
                        5
                    )
            pg.display.flip()

        pg.init()

        screen = pg.display.set_mode(
            (
                self.cell_size*self.env_size[0]+OFFSET,
                self.cell_size*self.env_size[1]+OFFSET
            )
        )
        font = pg.font.SysFont("comicsans", 30, True)
        score = [0 for i in range(nb_obj)]
        tmp_str = ""
        for i in range(len(score)):
            tmp_str += f"{score[i]},"
        text = font.render(f'Score: ({tmp_str})', True, (0,0,0))
        textRect = text.get_rect()
        textRect.center = (
            (self.cell_size*self.env_size[0]+OFFSET) // 2 ,
            self.cell_size*self.env_size[1]+ OFFSET//1.25
        )
        rectangles , rect_centers = [], []

        for x in range(self.env_size[0]):
            for y in range(self.env_size[1]):
                rect = pg.Rect(
                    x * (self.cell_size+1)+ OFFSET/2,
                    y * (self.cell_size+1)+ OFFSET/2,
                    self.cell_size,
                    self.cell_size
                )
                # The grid will be a list of (rect, color) tuples.
                if(self.state_set[x][y].obstacle):
                    rectangles.append((rect, self.color_obstacle, None))
                elif((x, y) in self.reward_states):
                    rectangles.append((
                        rect,
                        self.color_goal_state,
                        None
                    ))
                else:
                    rect_centers.append((rect.centerx, rect.centery))
                    rectangles.append((
                        rect,
                        self.color_normal_state,
                        self.state_set[x][y].circle_color
                    ))

        drawGrid(screen, rectangles)

        current_state_indices = (0,0)
        if(starting_state is not None
            and self.state_set[starting_state[0]][starting_state[1]].obstacle):
            current_state_indices = starting_state

        clock = pg.time.Clock()
        goal_reached = False
        while True:
            for event in pg.event.get():
                if event.type == pg.QUIT:
                    pg.quit()
                    sys.exit()
            drawGrid(screen, rectangles)
            screen.blit(text, textRect)
            i, j = current_state_indices
            current_state = rectangles[i*self.env_size[1]+j][0]

            if(policy is not None and not goal_reached):
                probas = [k[1] for k in policy[i][j]]
                actions = [k[0] for k in policy[i][j]]
                a = np.random.choice(actions, 1, p = probas)[0]
                possible_next_states = self.transition(self.state_set[i][j], a)
                probas = [k[1] for k in possible_next_states]
                state = [k[0] for k in possible_next_states]
                next_state = np.random.choice(state, 1, p = probas)[0]
                current_state_indices = self.__get_state_indices__(next_state)
                if(current_state_indices in self.reward_states):
                    goal_reached = True
                tmp_str = ""
                for i in range(nb_obj):
                    if(nb_obj <= 1):
                        score[i] += next_state.reward[i] - 1
                    else:
                        score[i] += next_state.reward[i]
                    tmp_str += f"{score[i]},"
                text = font.render(
                    f'Score: ({tmp_str})',
                     True,
                     (0,0,0)
                )
                #repaint old state with white
                pg.draw.circle(
                    screen,
                    (255,255,0),
                    (current_state.centerx, current_state.centery),
                    20
                )
            elif(goal_reached):
                pg.draw.circle(
                    screen,
                    (255,255,0),
                    (current_state.centerx, current_state.centery),
                    20
                )
            pg.display.update()
            clock.tick(0.5)

    def play_policy(self, policy = None, starting_state = None, nb_obj =1):
        score = [0 for i in range(nb_obj)]
        tmp_str = ""
        for i in range(len(score)):
            tmp_str += f"{score[i]},"


        current_state_indices = (0,0)
        if(starting_state is not None
            and self.state_set[starting_state[0]][starting_state[1]].obstacle):
            current_state_indices = starting_state

        clock = pg.time.Clock()
        goal_reached = False
        while not goal_reached:
            i, j = current_state_indices
            if(policy is not None and not goal_reached):
                probas = [k[1] for k in policy[i][j]]
                actions = [k[0] for k in policy[i][j]]
                a = np.random.choice(actions, 1, p = probas)[0]
                possible_next_states = self.transition(self.state_set[i][j], a)
                probas = [k[1] for k in possible_next_states]
                state = [k[0] for k in possible_next_states]
                next_state = np.random.choice(state, 1, p = probas)[0]
                current_state_indices = self.__get_state_indices__(next_state)
                if(current_state_indices in self.reward_states):
                    goal_reached = True
                tmp_str = ""
                for i in range(nb_obj):
                    if(nb_obj <= 1):
                        score[i] += next_state.reward[i] - 1
                    else:
                        score[i] += next_state.reward[i]
                #repaint old state with white
        return score
