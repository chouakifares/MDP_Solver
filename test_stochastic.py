from Classes import *
import time
import matplotlib.pyplot as plt


########### EX1 Q2.1 ################
sizes = [(10,5),(15, 10), (20, 15)]
nb_trials = 1
avg_solving_times = []
avg_nb_iter = []
for size in sizes:
    tmp = []
    tmp_iter = []
    for _ in range(nb_trials):
        mdp = MDP.create_mdp(size,[(size[0]-1,size[1]-1)], False, False)
        start_t = time.time()
        policy, nb_iter = mdp.solve(0.9)
        time_spent = time.time() - start_t
        tmp.append(time_spent)
        tmp_iter.append(nb_iter)
    
    avg_solving_times.append(sum(tmp)/nb_trials)
    avg_nb_iter.append(sum(tmp_iter)/nb_trials)


for i,size in enumerate(sizes):
    print(f"{size}  | time --> {avg_solving_times[i]} s")
    print(f"{size}  | iter --> {avg_nb_iter[i]} iterations")
