from Classes import *

mdp = MDP.create_mdp(
    env_size = (10,5),
    reward_states = [(9,4)],
    deterministic = False,
    multi_objective = False,
)
policy, nb_iter = mdp.solve(0.7)


nb_trials = 1000
scores = []
for _ in range(nb_trials):
    score = mdp.play_policy(policy = policy, nb_obj=1)
    scores.append(score[0])

print(f"Average score in {nb_trials} attempts = ", sum(scores)/nb_trials)
