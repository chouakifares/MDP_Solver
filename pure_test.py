from Classes import *
import numpy as np
import time

pure_time = []
nonpure_time = []
pure_score = []
nonpure_score = []
for _ in range(10):
    mdp = MDP.create_mdp(env_size = (20,15),reward_states = [(19,14)],deterministic = True,multi_objective = True,r = (20,20),seed = _)
    start = time.time()
    policy, nb_iter = mdp.solve(0.99, force_pure_policies=True)
    pure_time.append(time.time() - start)
    score = mdp.play_policy(policy = policy, nb_obj=2)
    print(score)
    pure_score.append(score)

    start = time.time()
    policy, nb_iter = mdp.solve(0.99, force_pure_policies=False)
    nonpure_time.append(time.time() - start)
    score = mdp.play_policy(policy = policy, nb_obj=2)
    print(score)
    nonpure_score.append(score)

print(sum(pure_time)/10)
print(sum(nonpure_time)/10)
print(pure_score)
print(nonpure_score)
