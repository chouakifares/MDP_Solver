from Classes import *

mdp = MDP.create_mdp(
    env_size = (15,10),
    reward_states = [(14,9)],
    deterministic = True,
    multi_objective = True,
    r = (1000,1000)
)
policy, nb_iter = mdp.solve(0.9, force_pure_policies = True)

mdp.display_env(policy = policy, nb_obj=1)
