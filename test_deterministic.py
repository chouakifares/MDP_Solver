from Classes import *
import time
import matplotlib.pyplot as plt

############ EX1 Q1 ################
# Cas déterministe:

# sizes = [(10,5), (15,10), (20, 15)]
sizes = [(10,15)]
gamma = 1
nb_trials = 5
solving_times = []
nb_iterations = []
for size in sizes:
    tmp = []
    tmp_iter = []
    for _ in range(nb_trials):
        mdp = MDP.create_mdp(size,[(size[0]-1,size[1]-1)], True, False)
        start_t = time.time()
        policy, nb_iter = mdp.solve(gamma)
        time_spent = time.time() - start_t
        tmp.append(time_spent)
        tmp_iter.append(nb_iter)

    solving_times.append(sum(tmp)/nb_trials)
    nb_iterations.append(sum(tmp_iter)/nb_trials)

for i,size in enumerate(sizes):
    print(f"{size} {gamma} | time --> {solving_times[i]} s")
    print(f"{size} {gamma} | iter --> {nb_iterations[i]} iterations")
