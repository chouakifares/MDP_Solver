from Classes import *
import time

############ EX2 Q1 ################

sizes = [(10,5),(15,10),(20,15)]
solving_times = []
nb_trials = 100
gamma = 0.7
for size in sizes:
    tmp = []
    for _ in range(nb_trials):
        mdp = MDP.create_mdp(
            env_size = size,
            reward_states=[(size[0]-1,size[1]-1)],
            deterministic = False,
            multi_objective = True,
            r = (1000,1000)
        )
        start_t = time.time()
        policy, nb_iter = mdp.solve(gamma)
        time_spent = time.time() - start_t
        tmp.append(time_spent)

    solving_times.append(tmp)

for i,size in enumerate(sizes):
    print(f"{size} | time --> {solving_times[i]} s")
