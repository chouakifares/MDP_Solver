# MDP Solver

In this project a solver for Markov decision processes(MDPs) to solve problems of planning and decison in stochastic environments. The first part of the project consists of using value iteration over an MDP in
order to ind the best policy for an agent that tries to find his path inside a maze. This maze can deterministic or non deterministic. The second part of this project is formulating the bi-criteria problem of finding a path that minimizes two criterias and then find the policy that gives the most balanced path ie the one that minimizes the minmax criterion over the objectives.
